﻿!  VP_04.f90 
!
!  FUNCTIONS:
!  VP_04 - Находит решение системы линейных уравнений
!

!****************************************************************************
!
!  PROGRAM: VP_04
!
!  PURPOSE:  Чтение и вывод данных, выбор метода
!
!****************************************************************************

    program VP_04
    use methods
    implicit none
    
    real(8), allocatable :: A(:,:), B(:), X(:)
    character(6) :: mettype
    integer(4) :: i, n, k

    open(1,file='data.dat')
    read(1,'(2x,I6)') n
        allocate(A(n,n),B(n),X(n))
    read(1,*) A
    read(1,*) B
    do i=1,n
        if (abs(A(i,i)) < (sum(abs(A(1:n,i)))-abs(A(i,i)))) then
            stop 'Диагонального преобладания нет'
        endif
    enddo    
    close (1)

    open(2,file='result.dat', status='replace')
    close (2)
   
    call getarg(1,mettype)

    select case (mettype)
        case ('yakobi')
            call yakobi(A,B,X,n)
            call output(n,X,mettype,A,B)
        case ('seidel')
            call seidel(A,B,X,n)
            call output(n,X,mettype,A,B)
        case ('relax')
            call relax(A,B,X,n)
            call output(n,X,mettype,A,B)
        case default
            mettype='all'
            write(*,*) 'Неверно указан ключ. &
            &По умолчанию будут выполнены все методы.'
            write(*,*) 'Для выбора метода гаусса используйте &
                &ключ gauss, для метода жордана jordan, &
                &для метода с выбором ведущего элемента choise'
            call yakobi(A,B,X,n)
            call output(n,X,mettype,A,B)
            call seidel(A,B,X,n)
            call output(n,X,mettype,A,B)
            call relax(A,B,X,n)
            call output(n,X,mettype,A,B)
        end select

    deallocate(A,B,X)
    end program VP_04
